package com.example.liyu_android_java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.Arrays;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_BOOK = "extra_book";

    Button btnTo;
    ListView listView;

    private Book[] books = {
            new Book("Book1","Author1","Publisher1",20.5),
            new Book("Book2","Author2","Publisher2",15.5),
            new Book("Book3","Author3","Publisher3",30.5),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("bookList+详细信息显示");

        Arrays.sort(books, Comparator.comparingDouble(Book::getPrice));
        //组件加载
        btnTo = findViewById(R.id.button_to);
        listView = findViewById(R.id.listView);

        ArrayAdapter<Book> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,books);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener((parent,view,position,id)->{
            Book selectedBook = books[position];
            Intent intent = new Intent(MainActivity.this,DetailActivity.class);
            intent.putExtra(EXTRA_BOOK,selectedBook);
            startActivity(intent);
        });

        btnTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,DetailActivity.class);
                startActivity(intent);
            }
        });
    }
}