package com.example.liyu_android_java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    Button btnReturn;
    TextView detailView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        setTitle("detail");

        // 组件加载
        btnReturn = findViewById(R.id.button_return);
        detailView = findViewById(R.id.detail_view);

        Book selectedBook = (Book) getIntent().getSerializableExtra(MainActivity.EXTRA_BOOK);
        if (selectedBook != null) {
            String bookDetails = "Title: " + selectedBook.getTitle() + "\n" +
                    "Author: " + selectedBook.getAuthor() + "\n" +
                    "Publisher: " + selectedBook.getPublisher() + "\n" +
                    "Price: $" + selectedBook.getPrice();
            detailView.setText(bookDetails);
        }

        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}